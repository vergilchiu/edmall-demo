create table product(
id int not null auto_increment,
name varchar(255),
brand varchar(255),
price int,
primary key(id)
);

insert into product values(1,"y7200","lenovo",6000);
insert into product values(2,"y7201","lenovo",5000);
insert into product values(3,"y7202","lenovo",4000);
insert into product values(4,"y7203","lenovo",2000);

create table user(
id int not null auto_increment,
username varchar(255),
password varchar(255),
primary key(id)
);

insert into user(username,password) values('admin','123');


create table order_info(
id int not null auto_increment,
user_id int,
username varchar(255),
address varchar(255),
order_time datetime,
primary key(id)
);

insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);
insert into order_info(user_id,username,address,order_time) values(1,"admin","苏州工业园区",CURRENT_TIMESTAMP);


create table order_detail(
id int not null auto_increment,
order_id int,
product_name varchar(255),
price int,
num int,
primary key(id)
);







