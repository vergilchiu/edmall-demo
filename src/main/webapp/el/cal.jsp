<%@page import="java.text.SimpleDateFormat"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>
	<%
		request.setAttribute("value1", 2);
		request.setAttribute("value2", 8);
		
		request.setAttribute("str1", "");
		request.setAttribute("str2", "test1");
	%>
	加:${value1+value2}
	<br> 乘:${value1*value2}
	<br>
	等于:${value1!=value2}
	<br>
	字符串等于:${str1==str2}
	<br>
	或：${str1==str2||value1<value2}
	<br>
	与：${str1==str2&&value1<value2}
	<br>
	empty:${empty(str1) }
	<br>
	条件:${str1==str2?"a":"b"}
	<%
	int a=0;
	int b=0;
	int c=a==b?0:1;
	%>
</body>
</html>
