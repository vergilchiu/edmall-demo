<%@page import="java.text.SimpleDateFormat"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>
	<%
		Map<String, String> map1 = new HashMap();
		map1.put("key1", "value1111");
		map1.put("key2", "value2222");
		pageContext.setAttribute("map", map1);

		Map<String, String> map = new HashMap();
		map.put("key1", "value1");
		map.put("key2", "value2");
		request.setAttribute("map", map);
	%>
    map:${requestScope.map.key1 }
    <br>
	map:${map.key1}
	<br>
	<c:forEach var="vo" items="${map}">
	${vo.key} -> ${vo.value }
	<br>
	</c:forEach>
	<br>
</body>
</html>
