<%@page import="java.text.SimpleDateFormat"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>
	cout:
	<%
	request.setAttribute("test", "test");
%>
	<c:out value="${test1}">mytest</c:out>
	<br>
	cset:
	<c:set var="test1" value="mytest1"></c:set>
	${test1}
	<br>
	cremove:
	<c:remove var="test1"/>
	${test1 }
	<br>
	catch:
	<c:catch var="myException">
	   ${1/1}
	</c:catch>
	${myExeption }
</body>
</html>
