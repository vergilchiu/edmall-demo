<%@page import="org.sgy.mall.model.User" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.Map.Entry" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <title>My JSP 'ShowDate.jsp' starting page</title>
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
</head>
<body>

<table border="1">
    <tr>
        <th>username</th>
        <th>gender</th>
    </tr>

    <%
        Object userMapObject = application.getAttribute("userMap");
        if (userMapObject != null) {
            HashMap<String, User> userMap = (HashMap<String, User>) userMapObject;
            if (userMap == null || userMap.size() == 0) {
                out.print("还没有用户登陆！");
            } else {
                for (Entry<String, User> entry : userMap.entrySet()) {
                    String userName = entry.getKey();
                    response.getWriter().println(userName);
    %>
    <tr>
        <td><%=userName%>
        </td>
        <td>$100</td>
    </tr>
    <%
                }
            }
        }
    %>
</table>
<br>
</body>
</html>
