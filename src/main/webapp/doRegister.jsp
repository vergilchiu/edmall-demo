<%@page import="org.sgy.mall.dao.impl.UserDaoImpl"%>
<%@page import="org.sgy.mall.model.User"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>
	<%
		String name = request.getParameter("txtUser");
		String pwd = request.getParameter("txtPassword");
		//注册用户，将用户信息放到数据库
		UserDaoImpl userdao = new UserDaoImpl();
		boolean isRegister = userdao.addUser(name, pwd);
		if (isRegister) {
			System.out.println("注册成功");
		}else{
			System.out.println("注册失败");
		}

		//注册成功，跳转到index页面
		User user = new User();
		user.setUserName(name);
		user.setPassword(pwd);
		session.setAttribute("user", user);
		response.addCookie(new Cookie("userName", name));
		response.addCookie(new Cookie("password", pwd));
		response.sendRedirect("./index.jsp");
	%>
	<br>
</body>
</html>
