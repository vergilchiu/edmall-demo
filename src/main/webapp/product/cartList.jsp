<%@page import="org.sgy.mall.model.Cart"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>
	<form action="orderServlet?type=add" method="post">
		<table border=1>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>price</th>
				<th>num</th>
				<th>total</th>
			</tr>

			<%
				//获取购物车数据
				Object object = session.getAttribute("cartList");
				List<Cart> cartList = new ArrayList();
				if (object != null) {
					cartList = (ArrayList<Cart>) object;
				}

				//计算购物车总价
				double sum = 0.0;

				for (Cart cart : cartList) {
			%>
			<tr>
				<td><input value=<%=cart.getId()%> name="productId"></td>
				<td><input value=<%=cart.getName()%> name="productName"></td>
				<td><input value=<%=cart.getPrice()%> name="price"></td>
				<td><input value=<%=cart.getNum()%> name="num"></td>
				<td><%=cart.getPrice() * cart.getNum()%></td>
			</tr>
			<%
				sum += cart.getPrice() * cart.getNum();
				}
			%>

			<tr>
				<td>总价:<%=sum%></td>
			</tr>
		</table>
		<input type="submit" value="提交订单">
	</form>
	<br>
</body>
</html>
