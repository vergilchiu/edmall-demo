<%@page import="org.sgy.mall.model.Product"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.sgy.mall.dao.impl.ProductDaoImpl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>

	<c:forEach items="${list}" var="vo">
		<form method="post" action="/edmall/CartServlet?type=add">
			<h1>产品1</h1>
			<ul>
				<li>id:${vo.id}</li>
				<li>商品名称：${vo.name}</li>
				<li>品牌：${vo.brand}</li>
				<li>价格:${vo.price}</li>
			</ul>
			<input type="hidden" name="id" value=${vo.id}> <input
				type="hidden" name="name" value=${vo.name}> <input
				type="hidden" name="brand" value=${vo.brand}> <input
				type="hidden" name="price" value=${vo.price}> 购买数量:<input
				name="num"> <input type="submit" value="添加到购物车">
		</form>
	</c:forEach>
</body>
</html>
