<%@page import="org.sgy.mall.model.OrderInfo"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@taglib prefix="myTag" uri="http://localhost/tags/myTags"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>


	<table border="1">
		<tr>
			<th>id</th>
			<th>username</th>
			<th>address</th>
			<th>orderTime</th>
		</tr>
		<c:forEach var="order" items="${list}" begin="${page.startIndex}" end="${page.endIndex}">
			<tr>
				<td>${order.id}</td>
				<td><${order.username}</td>
				<td>${order.address }</td>
				<td>${order.orderTime }</td>
			</tr>
		</c:forEach>
		<myTag:page page="${page}"></myTag:page>
	</table>
	<br>
</body>
</html>
