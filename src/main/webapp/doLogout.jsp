<%@page import="org.sgy.mall.model.User" %>
<%@page import="java.util.HashMap" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <title>My JSP 'ShowDate.jsp' starting page</title>
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
</head>
<body>
<%

    //从application中删除用户登录信息
    Object userObject = session.getAttribute("user");
    if (userObject != null) {
        User user = (User) userObject;
        Object userMapObject = application.getAttribute("userMap");
        if (userMapObject != null) {
            HashMap<String, User> userMap = (HashMap<String, User>) userMapObject;
            if (userMap != null && userMap.size() != 0) {
                userMap.remove(user.getUserName());
            }
        }
    }

    //注销session信息
    session.setMaxInactiveInterval(0);
    session.invalidate();
    response.sendRedirect("./login.jsp");

%>
</body>
</html>
