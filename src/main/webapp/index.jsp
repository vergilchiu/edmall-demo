<%@page import="org.sgy.mall.model.User" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">
    <title></title>
</head>
<body>
<%
    Object userObject = session.getAttribute("user");
    if (userObject != null) {
        User user = (User) userObject;
%>
欢迎回来,<%=user.getUserName()%><br>
<a href="./doLogout.jsp">退出</a>
<%
} else {
%>
<a href="./login.jsp">请登录</a>
<%
    }
%>
</body>
</html>
