<%@page import="org.sgy.mall.dao.impl.UserDaoImpl"%>
<%@page import="org.sgy.mall.model.User"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title></title>
</head>
<body>
	<%
		//获取登录参数
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("txtUser");
		String pwd = request.getParameter("txtPassword");
		String loginInfo = "";

		//登录验证,从数据库中验证账号密码
		UserDaoImpl userdao = new UserDaoImpl();
		boolean isLogin = userdao.login(name, pwd);
		if (!isLogin) {
			response.getWriter().println("用户名密码错误");
			return;
		}

		//把用户信息存储到session中
		User user = new User();
		user.setUserName(name);
		user.setPassword(pwd);
		session.setAttribute("user", user);
		response.addCookie(new Cookie("userName", name));
		response.addCookie(new Cookie("password", pwd));
		response.sendRedirect("./index.jsp");

		//统计在线用户
		HashMap<String, User> userMap = (HashMap<String, User>) application.getAttribute("userMap");
		if (userMap == null) {
			userMap = new HashMap<String, User>();
		}
		userMap.put(name, user);
		application.setAttribute("userMap", userMap);

		response.getWriter().println(loginInfo);
	%>


</body>
</html>
