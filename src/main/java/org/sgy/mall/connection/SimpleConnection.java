package org.sgy.mall.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SimpleConnection {

	public static void main(String[] args) throws Exception {
		// 加载驱动
		String driver = "com.mysql.jdbc.Driver";
		Class.forName(driver);

		// 连接数据库
		String url = "jdbc:mysql://localhost:3306/edmall?characterEncoding=utf-8";
		String user = "root";
		String password = "root";
		Connection connection = DriverManager.getConnection(url, user, password);

		PreparedStatement statement = null;
		String sql = null;

		// insert
		sql = "insert into product (name,brand,price) values('x100','dell',5000)";
		statement = connection.prepareStatement(sql);
		statement.execute();

		// delete
		sql = "delete from product where id=1";
		statement = connection.prepareStatement(sql);
		statement.execute();

		// update
		sql = "update product set price=10000 where id=2";
		statement = connection.prepareStatement(sql);
		statement.execute();

		// select
		sql = "select * from product";
		statement = connection.prepareStatement(sql);
		ResultSet rs = statement.executeQuery();
		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String brand = rs.getString("brand");
			int price = rs.getInt("price");

			System.out.println("id=" + id + ",name=" + name + ",brand=" + brand + ",price=" + price);
		}

		// close connection
		connection.close();
	}
}
