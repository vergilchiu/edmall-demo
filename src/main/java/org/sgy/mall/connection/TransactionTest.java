package org.sgy.mall.connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionTest {
	public static void main(String[] arsgs) {
		Connection con = null;
		try {
			con = DataSource.getConnection();
			String sql = "insert into user(username,password) values('3','4')";
			String sql2 = "update product set name='x7200' where id=1";
			con.setAutoCommit(false);
			// 执行sql语句
			// sql
			PreparedStatement statement = con.prepareStatement(sql);
			statement.execute();
			// sql2
			statement = con.prepareStatement(sql2);
			statement.execute();
			con.commit();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
