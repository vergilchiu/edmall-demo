package org.sgy.mall.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.sgy.mall.model.Page;

public class PageTag extends TagSupport {
	private Page page;

	@Override
	public int doStartTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			String str = "<tr><td>当前页:%d</td><td>当前页数量:%d</td><td>一共：%d</td></tr>";
			str = String.format(str, page.getPage(), page.getLimit(), page.getTotal());
			out.println(str);
		} catch (Exception e) {

		}
		return SKIP_BODY;
	}

	@Override
	public int doEndTag() throws JspException {
		return Tag.SKIP_PAGE;
	}

	public void setPage(Page page) {
		this.page = page;
	}
}
