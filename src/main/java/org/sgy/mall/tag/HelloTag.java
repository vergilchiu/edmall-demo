package org.sgy.mall.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

public class HelloTag extends TagSupport {
	private String name;

	@Override
	public int doStartTag() throws JspException {
		try {
			JspWriter out = pageContext.getOut();
			out.println("hello tag,my name is:" + name);
		} catch (Exception e) {

		}
		return SKIP_BODY;
	}
	
	@Override
	public int doEndTag() throws JspException {
		return Tag.SKIP_PAGE;
	}

	public void setName(String name) {
		this.name = name;
	}
}
