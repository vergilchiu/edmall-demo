package org.sgy.mall.model;

public class Page {
	private int page;
	private int limit;
	private int total;

	public int getStartIndex() {
		int index = (page - 1) * limit;
		return index;
	}

	public int getEndIndex() {
		int index = page * limit - 1;
		return index;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
