package org.sgy.mall.utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailTest {

	public static void main(String[] args) throws Exception {
		Properties properties = new Properties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.host", "smtp.163.com");
		// properties.put("mail.smtp.port", "smtp.163.com");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.debug", "true");

		// 获得发送邮箱会话
		Session session = Session.getInstance(properties);
		// 构建邮件
		Message message = new MimeMessage(session);
		// 发件人
		message.setFrom(new InternetAddress("sugongyuan2019@163.com"));
		// 收件人
		message.setRecipients(Message.RecipientType.TO,
				new InternetAddress[] { new InternetAddress("zhaoweidt163@163.com") });
		// 邮箱标题
		message.setSubject("苏工院教务处");
		// 邮件内容
		message.setText("国庆放假7天");

		// 发送邮件
		Transport transport = session.getTransport();
		transport.connect("sugongyuan2019@163.com", "123abc");
		transport.sendMessage(message, message.getAllRecipients());
		
		transport.close();
	}
}
