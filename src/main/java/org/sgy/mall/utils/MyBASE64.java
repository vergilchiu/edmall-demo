package org.sgy.mall.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class MyBASE64 {

	public static void main(String[] args) {
		// base64 编码
		String str = "this is base64";
		String base64Str = Base64.encodeBase64String(str.getBytes());
		System.out.println("base64 encode:" + base64Str);
		// base 解码
		byte[] base64Bytes = Base64.decodeBase64(base64Str);
		System.out.println(new String(base64Bytes));
	}
}
