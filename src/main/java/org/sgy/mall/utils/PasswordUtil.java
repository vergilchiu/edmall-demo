package org.sgy.mall.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordUtil {
	private static String myKey = "this is my key";

	public static String encryPassword(String password) {
		password = password + myKey;
		String encryPassword = DigestUtils.md5Hex(password);
		return encryPassword;
	}
}
