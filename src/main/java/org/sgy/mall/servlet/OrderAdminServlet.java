package org.sgy.mall.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.servlet.ServletUtilities;
import org.sgy.mall.dao.impl.ProductDaoImpl;

public class OrderAdminServlet extends HttpServlet{
   
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String type=req.getParameter("type");
		if(type.equals("stat")) {
			String graphType=req.getParameter("graphType");
			String top=req.getParameter("top");
			String field=req.getParameter("field");
			ProductDaoImpl dao = new ProductDaoImpl();
			JFreeChart chart=dao.statOrder(Integer.parseInt(top), graphType, field);
			StandardEntityCollection sec = new StandardEntityCollection();
			ChartRenderingInfo info = new ChartRenderingInfo(sec);
			String filename=ServletUtilities.saveChartAsPNG(chart, 500, 600, req.getSession());
			resp.sendRedirect("/edmall/product/statOrder.jsp?filename="+filename);
		}
	}
}
