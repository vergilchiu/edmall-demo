package org.sgy.mall.dao;

import java.util.List;

import org.sgy.mall.model.OrderInfo;

public interface OrderDao {
	boolean add(OrderInfo orderInfo);

	List<OrderInfo> list();
}
