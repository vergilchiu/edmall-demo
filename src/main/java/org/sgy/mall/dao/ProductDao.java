package org.sgy.mall.dao;

import java.util.List;

import org.jfree.chart.JFreeChart;
import org.sgy.mall.model.Product;

public interface ProductDao {

	List<Product> fetchList();
	
	boolean add(Product product);
	
    JFreeChart statOrder(int top,String graphType,String field);
}
