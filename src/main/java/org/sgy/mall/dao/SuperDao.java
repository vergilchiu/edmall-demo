package org.sgy.mall.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.sgy.mall.connection.ConnectionManager;
import org.sgy.mall.connection.DataSource;;

public class SuperDao {
	protected Connection connection;

	public SuperDao() {
		// ConnectionManager connectionManager = new ConnectionManager();
		// Connection connection2 = connectionManager.getConnection();
		// 通过连接池获取连接
		Connection connection2 = null;
		try {
			connection2 = DataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.connection = connection2;
	}
}
