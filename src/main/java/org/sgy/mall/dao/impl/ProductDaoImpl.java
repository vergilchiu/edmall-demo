package org.sgy.mall.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieToolTipGenerator;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.sgy.mall.dao.ProductDao;
import org.sgy.mall.dao.SuperDao;
import org.sgy.mall.model.Product;

public class ProductDaoImpl extends SuperDao implements ProductDao {

	public List<Product> fetchList() {
		String sql = "select * from product order by id";
		ArrayList<Product> list = new ArrayList<Product>();
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String brand = rs.getString("brand");
				int price = rs.getInt("price");

				Product product = new Product();
				product.setId(id);
				product.setName(name);
				product.setBrand(brand);
				product.setPrice(price);

				list.add(product);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static void main(String[] args) {
		ProductDaoImpl productDaoImpl = new ProductDaoImpl();
		List<Product> fetchList = productDaoImpl.fetchList();
		System.out.println(fetchList.size());
	}

	@Override
	public boolean add(Product product) {
		String sql = "insert into product (name,brand,price) values(?,?,?)";
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, product.getName());
			statement.setString(2, product.getBrand());
			statement.setInt(3, product.getPrice());
			int i = statement.executeUpdate();
			if (i > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	@Override
	public JFreeChart statOrder(int top, String graphType, String field) {
		JFreeChart chart=null;
		String title="";
		String x="";
		String sql="";
		if(field.equals("book")) {
			sql="select product_name,sum(num) as num from order_detail group by product_name order by num limit "+top;
			title="订单销售情况";
		}else {
			
		}
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			if(graphType.equals("pie")) {
				DefaultPieDataset data = new DefaultPieDataset();
				while(rs.next()) {
					data.setValue(rs.getString(1), rs.getInt(2));
				}
				PiePlot3D plot = new PiePlot3D(data);
				chart=new JFreeChart("",JFreeChart.DEFAULT_TITLE_FONT,plot,true);
				chart.setBackgroundPaint(java.awt.Color.LIGHT_GRAY);
				chart.setTitle(title);
				plot.setToolTipGenerator(new StandardPieToolTipGenerator());
			}else if(graphType.equals("column")) {
				DefaultCategoryDataset data = new DefaultCategoryDataset();
				while(rs.next()) {
                    data.addValue(rs.getInt(2), title, rs.getString(1));
				}
				chart=ChartFactory.createBarChart(title, x, "数量", data,PlotOrientation.VERTICAL,false,false,false);
			}
			
		}catch(Exception e) {
			
		}
		return chart;
	}
}
