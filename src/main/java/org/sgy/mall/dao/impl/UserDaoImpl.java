package org.sgy.mall.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.sgy.mall.dao.SuperDao;
import org.sgy.mall.dao.UserDao;
import org.sgy.mall.utils.PasswordUtil;

public class UserDaoImpl extends SuperDao implements UserDao {

	@Override
	public boolean login(String username, String password) {
		String sql = "select * from user where username='%s' and password='%s'";
		sql = String.format(sql, username, password);
		try {
			PreparedStatement prepareStatement = connection.prepareStatement(sql);
			ResultSet rs = prepareStatement.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean addUser(String username, String password) {
		String newPassword=PasswordUtil.encryPassword(password);
		String sql = "insert into user (username,password) values('%s','%s')";
		sql = String.format(sql, username, newPassword);
		try {
			PreparedStatement prepareStatement = connection.prepareStatement(sql);
			boolean execute = prepareStatement.execute();
			return execute;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
