package org.sgy.mall.dao;

public interface UserDao {

	boolean login(String username, String password);
	
	boolean addUser(String username,String password);
}
